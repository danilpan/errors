package http

import (
	"gitlab.com/danilpan/errors/http/fivexx"
	"gitlab.com/danilpan/errors/http/fourxx"
	"io"
	"net/http"
	"reflect"
	"strings"
)

const (
	StatusAuthenticationTimeout  = 419
	StatusRetryWith              = 449
	StatusClientClosedRequest    = 499
	StatusBandwidthLimitExceeded = 509
	StatusUnknown                = 520
	StatusWebServerIsDown        = 521
	StatusConnectionTimedOut     = 522
	StatusOriginIsUnreachable    = 523
	StatusTimeoutOccurred        = 524
	StatusSSLHandshakeFailed     = 525
	StatusInvalidSSLCertificate  = 526
)

const (
	ErrorBadRequest = iota
	ErrorUnauthorized
	ErrorPaymentRequired
	ErrorForbidden
	ErrorNotFound
	ErrorMethodNotAllowed
	ErrorNotAcceptable
	ErrorProxyAuthenticationRequired
	ErrorRequestTimeout
	ErrorConflict
	ErrorGone
	ErrorLengthRequired
	ErrorPreconditionFailed
	ErrorPayloadTooLarge
	ErrorURITooLong
	ErrorUnsupportedMediaType
	ErrorRangeNotSatisfiable
	ErrorExpectationFailed
	ErrorTeapot
	ErrorAuthenticationTimeout
	ErrorMisdirectedRequest
	ErrorUnprocessableEntity
	ErrorLocked
	ErrorFailedDependency
	ErrorTooEarly
	ErrorUpgradeRequired
	ErrorPreconditionRequired
	ErrorTooManyRequests
	ErrorRequestHeaderFieldsTooLarge
	ErrorRetryWith
	ErrorUnavailableForLegalReasons
	ErrorClientClosedRequest
	ErrorInternalServerError
	ErrorNotImplemented
	ErrorBadGateway
	ErrorServiceUnavailable
	ErrorGatewayTimeout
	ErrorHTTPVersionNotSupported
	ErrorVariantAlsoNegotiates
	ErrorInsufficientStorage
	ErrorLoopDetected
	ErrorBandwidthLimitExceeded
	ErrorNotExtended
	ErrorNetworkAuthenticationRequired
	ErrorUnknownError
	ErrorWebServerIsDown
	ErrorConnectionTimedOut
	ErrorOriginIsUnreachable
	ErrorTimeoutOccurred
	ErrorSSLHandshakeFailed
	ErrorInvalidSSLCertificate
)

type ErrorMap map[string]int

var errorMap ErrorMap

func init() {
	errorMap = make(map[string]int)

	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorBadRequest{}).String())] = ErrorBadRequest
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorUnauthorized{}).String())] = ErrorUnauthorized
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorPaymentRequired{}).String())] = ErrorPaymentRequired
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorForbidden{}).String())] = ErrorForbidden
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorNotFound{}).String())] = ErrorNotFound
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorMethodNotAllowed{}).String())] = ErrorMethodNotAllowed
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorNotAcceptable{}).String())] = ErrorNotAcceptable
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorProxyAuthenticationRequired{}).String())] = ErrorProxyAuthenticationRequired
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorRequestTimeout{}).String())] = ErrorRequestTimeout
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorConflict{}).String())] = ErrorConflict
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorGone{}).String())] = ErrorGone
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorLengthRequired{}).String())] = ErrorLengthRequired
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorPreconditionFailed{}).String())] = ErrorPreconditionFailed
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorPayloadTooLarge{}).String())] = ErrorPayloadTooLarge
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorURITooLong{}).String())] = ErrorURITooLong
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorUnsupportedMediaType{}).String())] = ErrorUnsupportedMediaType
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorRangeNotSatisfiable{}).String())] = ErrorRangeNotSatisfiable
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorExpectationFailed{}).String())] = ErrorExpectationFailed
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorTeapot{}).String())] = ErrorTeapot
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorAuthenticationTimeout{}).String())] = ErrorAuthenticationTimeout
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorMisdirectedRequest{}).String())] = ErrorMisdirectedRequest
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorUnprocessableEntity{}).String())] = ErrorUnprocessableEntity
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorLocked{}).String())] = ErrorLocked
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorFailedDependency{}).String())] = ErrorFailedDependency
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorTooEarly{}).String())] = ErrorTooEarly
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorUpgradeRequired{}).String())] = ErrorUpgradeRequired
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorPreconditionRequired{}).String())] = ErrorPreconditionRequired
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorTooManyRequests{}).String())] = ErrorTooManyRequests
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorRequestHeaderFieldsTooLarge{}).String())] = ErrorRequestHeaderFieldsTooLarge
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorRetryWith{}).String())] = ErrorRetryWith
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorUnavailableForLegalReasons{}).String())] = ErrorUnavailableForLegalReasons
	errorMap[removeDotPrefix(reflect.TypeOf(fourxx.ErrorClientClosedRequest{}).String())] = ErrorClientClosedRequest
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorInternalServerError{}).String())] = ErrorInternalServerError
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorNotImplemented{}).String())] = ErrorNotImplemented
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorBadGateway{}).String())] = ErrorBadGateway
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorServiceUnavailable{}).String())] = ErrorServiceUnavailable
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorGatewayTimeout{}).String())] = ErrorGatewayTimeout
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorHTTPVersionNotSupported{}).String())] = ErrorHTTPVersionNotSupported
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorVariantAlsoNegotiates{}).String())] = ErrorVariantAlsoNegotiates
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorInsufficientStorage{}).String())] = ErrorInsufficientStorage
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorLoopDetected{}).String())] = ErrorLoopDetected
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorBandwidthLimitExceeded{}).String())] = ErrorBandwidthLimitExceeded
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorNotExtended{}).String())] = ErrorNotExtended
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorNetworkAuthenticationRequired{}).String())] = ErrorNetworkAuthenticationRequired
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorUnknownError{}).String())] = ErrorUnknownError
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorWebServerIsDown{}).String())] = ErrorWebServerIsDown
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorConnectionTimedOut{}).String())] = ErrorConnectionTimedOut
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorOriginIsUnreachable{}).String())] = ErrorOriginIsUnreachable
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorTimeoutOccurred{}).String())] = ErrorTimeoutOccurred
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorSSLHandshakeFailed{}).String())] = ErrorSSLHandshakeFailed
	errorMap[removeDotPrefix(reflect.TypeOf(fivexx.ErrorInvalidSSLCertificate{}).String())] = ErrorInvalidSSLCertificate
}

type CommonError struct {
	Message string `json:"message"`
}

func (c CommonError) Error() string {
	return c.Message
}

func DetermineResponseStatus(err error) int {
	errType := removeDotPrefix(reflect.TypeOf(err).String())
	n, ok := errorMap[errType]
	if !ok {
		return http.StatusInternalServerError
	}
	switch n {
	case ErrorBadRequest:
		return http.StatusBadRequest
	case ErrorUnauthorized:
		return http.StatusUnauthorized
	case ErrorPaymentRequired:
		return http.StatusPaymentRequired
	case ErrorForbidden:
		return http.StatusForbidden
	case ErrorNotFound:
		return http.StatusNotFound
	case ErrorMethodNotAllowed:
		return http.StatusMethodNotAllowed
	case ErrorNotAcceptable:
		return http.StatusNotAcceptable
	case ErrorProxyAuthenticationRequired:
		return http.StatusProxyAuthRequired
	case ErrorRequestTimeout:
		return http.StatusRequestTimeout
	case ErrorConflict:
		return http.StatusConflict
	case ErrorGone:
		return http.StatusGone
	case ErrorLengthRequired:
		return http.StatusLengthRequired
	case ErrorPreconditionFailed:
		return http.StatusPreconditionFailed
	case ErrorPayloadTooLarge:
		return http.StatusRequestEntityTooLarge
	case ErrorURITooLong:
		return http.StatusRequestURITooLong
	case ErrorUnsupportedMediaType:
		return http.StatusUnsupportedMediaType
	case ErrorRangeNotSatisfiable:
		return http.StatusRequestedRangeNotSatisfiable
	case ErrorExpectationFailed:
		return http.StatusExpectationFailed
	case ErrorTeapot:
		return http.StatusTeapot
	case ErrorAuthenticationTimeout:
		return StatusAuthenticationTimeout
	case ErrorMisdirectedRequest:
		return http.StatusMisdirectedRequest
	case ErrorUnprocessableEntity:
		return http.StatusUnprocessableEntity
	case ErrorLocked:
		return http.StatusLocked
	case ErrorFailedDependency:
		return http.StatusFailedDependency
	case ErrorTooEarly:
		return http.StatusTooEarly
	case ErrorUpgradeRequired:
		return http.StatusUpgradeRequired
	case ErrorPreconditionRequired:
		return http.StatusPreconditionRequired
	case ErrorTooManyRequests:
		return http.StatusTooManyRequests
	case ErrorRequestHeaderFieldsTooLarge:
		return http.StatusRequestHeaderFieldsTooLarge
	case ErrorRetryWith:
		return StatusRetryWith
	case ErrorUnavailableForLegalReasons:
		return http.StatusUnavailableForLegalReasons
	case ErrorClientClosedRequest:
		return StatusClientClosedRequest
	case ErrorInternalServerError:
		return http.StatusInternalServerError
	case ErrorNotImplemented:
		return http.StatusNotImplemented
	case ErrorBadGateway:
		return http.StatusBadGateway
	case ErrorServiceUnavailable:
		return http.StatusServiceUnavailable
	case ErrorGatewayTimeout:
		return http.StatusGatewayTimeout
	case ErrorHTTPVersionNotSupported:
		return http.StatusHTTPVersionNotSupported
	case ErrorVariantAlsoNegotiates:
		return http.StatusVariantAlsoNegotiates
	case ErrorInsufficientStorage:
		return http.StatusInsufficientStorage
	case ErrorLoopDetected:
		return http.StatusLoopDetected
	case ErrorBandwidthLimitExceeded:
		return StatusBandwidthLimitExceeded
	case ErrorNotExtended:
		return http.StatusNotExtended
	case ErrorNetworkAuthenticationRequired:
		return http.StatusNetworkAuthenticationRequired
	case ErrorUnknownError:
		return StatusUnknown
	case ErrorWebServerIsDown:
		return StatusWebServerIsDown
	case ErrorConnectionTimedOut:
		return StatusConnectionTimedOut
	case ErrorOriginIsUnreachable:
		return StatusOriginIsUnreachable
	case ErrorTimeoutOccurred:
		return StatusTimeoutOccurred
	case ErrorSSLHandshakeFailed:
		return StatusSSLHandshakeFailed
	case ErrorInvalidSSLCertificate:
		return StatusInvalidSSLCertificate
	default:
		return http.StatusInternalServerError
	}
}

func DetermineResponseStatusWithError(err error) (int, error) {
	return DetermineResponseStatus(err), &CommonError{Message: err.Error()}
}

func DetermineErrorFromResponseStatus(status int) error {
	return DetermineErrorFromResponseStatusWithMessage(status, "")
}

func DetermineErrorFromResponseStatusReader(status int, reader io.Reader) error {
	data, _ := io.ReadAll(reader)
	return DetermineErrorFromResponseStatusWithMessage(status, string(data))
}

func DetermineErrorFromResponseStatusBytes(status int, data []byte) error {
	return DetermineErrorFromResponseStatusWithMessage(status, string(data))
}

func DetermineErrorFromResponseStatusWithMessage(status int, message string) (err error) {
	switch status {
	case http.StatusBadRequest:
		err = &fourxx.ErrorBadRequest{Message: message}
	case http.StatusUnauthorized:
		err = &fourxx.ErrorUnauthorized{Message: message}
	case http.StatusPaymentRequired:
		err = &fourxx.ErrorPaymentRequired{Message: message}
	case http.StatusForbidden:
		err = &fourxx.ErrorForbidden{Message: message}
	case http.StatusNotFound:
		err = &fourxx.ErrorNotFound{Message: message}
	case http.StatusMethodNotAllowed:
		err = &fourxx.ErrorMethodNotAllowed{Message: message}
	case http.StatusNotAcceptable:
		err = &fourxx.ErrorNotAcceptable{Message: message}
	case http.StatusProxyAuthRequired:
		err = &fourxx.ErrorProxyAuthenticationRequired{Message: message}
	case http.StatusRequestTimeout:
		err = &fourxx.ErrorRequestTimeout{Message: message}
	case http.StatusConflict:
		err = &fourxx.ErrorConflict{Message: message}
	case http.StatusGone:
		err = &fourxx.ErrorGone{Message: message}
	case http.StatusLengthRequired:
		err = &fourxx.ErrorLengthRequired{Message: message}
	case http.StatusPreconditionFailed:
		err = &fourxx.ErrorPreconditionFailed{Message: message}
	case http.StatusRequestEntityTooLarge:
		err = &fourxx.ErrorPayloadTooLarge{Message: message}
	case http.StatusRequestURITooLong:
		err = &fourxx.ErrorURITooLong{Message: message}
	case http.StatusUnsupportedMediaType:
		err = &fourxx.ErrorUnsupportedMediaType{Message: message}
	case http.StatusRequestedRangeNotSatisfiable:
		err = &fourxx.ErrorRangeNotSatisfiable{Message: message}
	case http.StatusExpectationFailed:
		err = &fourxx.ErrorExpectationFailed{Message: message}
	case http.StatusTeapot:
		err = &fourxx.ErrorTeapot{Message: message}
	case StatusAuthenticationTimeout:
		err = &fourxx.ErrorAuthenticationTimeout{Message: message}
	case http.StatusMisdirectedRequest:
		err = &fourxx.ErrorMisdirectedRequest{Message: message}
	case http.StatusUnprocessableEntity:
		err = &fourxx.ErrorUnprocessableEntity{Message: message}
	case http.StatusLocked:
		err = &fourxx.ErrorLocked{Message: message}
	case http.StatusFailedDependency:
		err = &fourxx.ErrorFailedDependency{Message: message}
	case http.StatusTooEarly:
		err = &fourxx.ErrorTooEarly{Message: message}
	case http.StatusUpgradeRequired:
		err = &fourxx.ErrorUpgradeRequired{Message: message}
	case http.StatusPreconditionRequired:
		err = &fourxx.ErrorPreconditionRequired{Message: message}
	case http.StatusTooManyRequests:
		err = &fourxx.ErrorTooManyRequests{Message: message}
	case http.StatusRequestHeaderFieldsTooLarge:
		err = &fourxx.ErrorRequestHeaderFieldsTooLarge{Message: message}
	case StatusRetryWith:
		err = &fourxx.ErrorRetryWith{Message: message}
	case http.StatusUnavailableForLegalReasons:
		err = &fourxx.ErrorUnavailableForLegalReasons{Message: message}
	case StatusClientClosedRequest:
		err = &fourxx.ErrorClientClosedRequest{Message: message}
	case http.StatusInternalServerError:
		err = &fivexx.ErrorInternalServerError{Message: message}
	case http.StatusNotImplemented:
		err = &fivexx.ErrorNotImplemented{Message: message}
	case http.StatusBadGateway:
		err = &fivexx.ErrorBadGateway{Message: message}
	case http.StatusServiceUnavailable:
		err = &fivexx.ErrorServiceUnavailable{Message: message}
	case http.StatusGatewayTimeout:
		err = &fivexx.ErrorGatewayTimeout{Message: message}
	case http.StatusHTTPVersionNotSupported:
		err = &fivexx.ErrorHTTPVersionNotSupported{Message: message}
	case http.StatusVariantAlsoNegotiates:
		err = &fivexx.ErrorVariantAlsoNegotiates{Message: message}
	case http.StatusInsufficientStorage:
		err = &fivexx.ErrorInsufficientStorage{Message: message}
	case http.StatusLoopDetected:
		err = &fivexx.ErrorLoopDetected{Message: message}
	case StatusBandwidthLimitExceeded:
		err = &fivexx.ErrorBandwidthLimitExceeded{Message: message}
	case http.StatusNotExtended:
		err = &fivexx.ErrorNotExtended{Message: message}
	case http.StatusNetworkAuthenticationRequired:
		err = &fivexx.ErrorNetworkAuthenticationRequired{Message: message}
	case StatusUnknown:
		err = &fivexx.ErrorUnknownError{Message: message}
	case StatusWebServerIsDown:
		err = &fivexx.ErrorWebServerIsDown{Message: message}
	case StatusConnectionTimedOut:
		err = &fivexx.ErrorConnectionTimedOut{Message: message}
	case StatusOriginIsUnreachable:
		err = &fivexx.ErrorOriginIsUnreachable{Message: message}
	case StatusTimeoutOccurred:
		err = &fivexx.ErrorTimeoutOccurred{Message: message}
	case StatusSSLHandshakeFailed:
		err = &fivexx.ErrorSSLHandshakeFailed{Message: message}
	case StatusInvalidSSLCertificate:
		err = &fivexx.ErrorInvalidSSLCertificate{Message: message}
	default:
		return &fivexx.ErrorInternalServerError{Message: message}
	}
	return
}

func removeDotPrefix(name string) string {
	return name[strings.Index(name, ".")+1:]
}
