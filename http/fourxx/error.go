package fourxx

type ErrorBadRequest struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorBadRequest) Error() string {
	return e.Message
}

type ErrorUnauthorized struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorUnauthorized) Error() string {
	return e.Message
}

type ErrorPaymentRequired struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorPaymentRequired) Error() string {
	return e.Message
}

type ErrorForbidden struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorForbidden) Error() string {
	return e.Message
}

type ErrorNotFound struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorNotFound) Error() string {
	return e.Message
}

type ErrorMethodNotAllowed struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorMethodNotAllowed) Error() string {
	return e.Message
}

type ErrorNotAcceptable struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorNotAcceptable) Error() string {
	return e.Message
}

type ErrorProxyAuthenticationRequired struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorProxyAuthenticationRequired) Error() string {
	return e.Message
}

type ErrorRequestTimeout struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorRequestTimeout) Error() string {
	return e.Message
}

type ErrorConflict struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorConflict) Error() string {
	return e.Message
}

type ErrorGone struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorGone) Error() string {
	return e.Message
}

type ErrorLengthRequired struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorLengthRequired) Error() string {
	return e.Message
}

type ErrorPreconditionFailed struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorPreconditionFailed) Error() string {
	return e.Message
}

type ErrorPayloadTooLarge struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorPayloadTooLarge) Error() string {
	return e.Message
}

type ErrorURITooLong struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorURITooLong) Error() string {
	return e.Message
}

type ErrorUnsupportedMediaType struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorUnsupportedMediaType) Error() string {
	return e.Message
}

type ErrorRangeNotSatisfiable struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorRangeNotSatisfiable) Error() string {
	return e.Message
}

type ErrorExpectationFailed struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorExpectationFailed) Error() string {
	return e.Message
}

type ErrorTeapot struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorTeapot) Error() string {
	return e.Message
}

type ErrorAuthenticationTimeout struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorAuthenticationTimeout) Error() string {
	return e.Message
}

type ErrorMisdirectedRequest struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorMisdirectedRequest) Error() string {
	return e.Message
}

type ErrorUnprocessableEntity struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorUnprocessableEntity) Error() string {
	return e.Message
}

type ErrorLocked struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorLocked) Error() string {
	return e.Message
}

type ErrorFailedDependency struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorFailedDependency) Error() string {
	return e.Message
}

type ErrorUpgradeRequired struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorUpgradeRequired) Error() string {
	return e.Message
}

type ErrorPreconditionRequired struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorPreconditionRequired) Error() string {
	return e.Message
}

type ErrorTooManyRequests struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorTooManyRequests) Error() string {
	return e.Message
}

type ErrorRequestHeaderFieldsTooLarge struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorRequestHeaderFieldsTooLarge) Error() string {
	return e.Message
}

type ErrorRetryWith struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorRetryWith) Error() string {
	return e.Message
}

type ErrorUnavailableForLegalReasons struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorUnavailableForLegalReasons) Error() string {
	return e.Message
}

type ErrorClientClosedRequest struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorClientClosedRequest) Error() string {
	return e.Message
}

type ErrorTooEarly struct {
	Message string `json:"message,omitempty"`
}

func (e ErrorTooEarly) Error() string {
	return e.Message
}
