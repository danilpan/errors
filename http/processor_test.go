package http

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/danilpan/errors/http/fivexx"
	"gitlab.com/danilpan/errors/http/fourxx"
	"net/http"
	"testing"
)

func TestDetermineResponseStatus_BadRequest(t *testing.T) {
	br := &fourxx.ErrorBadRequest{Message: "test"}

	status := DetermineResponseStatus(br)

	assert.Equal(t, http.StatusBadRequest, status)
}

func TestDetermineResponseStatus_Teapot(t *testing.T) {
	br := &fourxx.ErrorTeapot{}

	status := DetermineResponseStatus(br)

	assert.Equal(t, http.StatusTeapot, status)
}

func TestDetermineResponseStatus_TooEarly(t *testing.T) {
	br := &fourxx.ErrorTooEarly{}

	status := DetermineResponseStatus(br)

	assert.Equal(t, http.StatusTooEarly, status)
}

func TestDetermineResponseStatus_Conflict(t *testing.T) {
	br := &fourxx.ErrorConflict{}

	status := DetermineResponseStatus(br)

	assert.Equal(t, http.StatusConflict, status)
}

func TestDetermineResponseStatus_NotExtended(t *testing.T) {
	br := &fivexx.ErrorNotExtended{}

	status := DetermineResponseStatus(br)

	assert.Equal(t, http.StatusNotExtended, status)
}

func TestDetermineResponseStatus_UnknownError(t *testing.T) {
	br := &fivexx.ErrorUnknownError{}

	status := DetermineResponseStatus(br)

	assert.Equal(t, StatusUnknown, status)
}

func TestDetermineErrorFromResponseStatusWithMessage_Unauthorized(t *testing.T) {
	msg := "Test"
	err := DetermineErrorFromResponseStatusWithMessage(http.StatusUnauthorized, msg)

	_, ok := err.(*fourxx.ErrorUnauthorized)

	assert.NotNil(t, err)
	assert.True(t, ok)
	assert.Equal(t, msg, err.Error())
}

func TestDetermineErrorFromResponseStatusWithMessage_Teapot(t *testing.T) {
	msg := "Test"
	err := DetermineErrorFromResponseStatusWithMessage(http.StatusTeapot, msg)

	_, ok := err.(*fourxx.ErrorTeapot)

	assert.NotNil(t, err)
	assert.True(t, ok)
	assert.Equal(t, msg, err.Error())
}

func TestDetermineErrorFromResponseStatus_AccessDenied(t *testing.T) {
	err := DetermineErrorFromResponseStatus(http.StatusUnauthorized)

	_, ok := err.(*fourxx.ErrorUnauthorized)

	assert.NotNil(t, err)
	assert.True(t, ok)
}

func TestDetermineErrorFromResponseStatus_ServiceUnavailable(t *testing.T) {
	err := DetermineErrorFromResponseStatus(http.StatusServiceUnavailable)

	_, ok := err.(*fivexx.ErrorServiceUnavailable)

	assert.NotNil(t, err)
	assert.True(t, ok)
}

func TestDetermineErrorFromResponseStatusReader_StatusConflict(t *testing.T) {
	data, _ := json.Marshal(&struct {
		Error string `json:"error"`
	}{Error: "Test"})
	buff := bytes.NewBuffer(data)

	err := DetermineErrorFromResponseStatusReader(http.StatusConflict, buff)

	assert.NotNil(t, err)
	assert.True(t, len(err.Error()) > 0)
}

func TestDetermineErrorFromResponseStatusReader_NotFound(t *testing.T) {
	data, _ := json.Marshal(&struct {
		Error string `json:"error"`
	}{Error: "Test"})

	err := DetermineErrorFromResponseStatusBytes(http.StatusNotFound, data)

	assert.NotNil(t, err)
	assert.True(t, len(err.Error()) > 0)
}
