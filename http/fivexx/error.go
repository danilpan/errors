package fivexx

type ErrorInternalServerError struct {
	Message string `json:"message"`
}

func (e ErrorInternalServerError) Error() string {
	return e.Message
}

type ErrorNotImplemented struct {
	Message string `json:"message"`
}

func (e ErrorNotImplemented) Error() string {
	return e.Message
}

type ErrorBadGateway struct {
	Message string `json:"message"`
}

func (e ErrorBadGateway) Error() string {
	return e.Message
}

type ErrorServiceUnavailable struct {
	Message string `json:"message"`
}

func (e ErrorServiceUnavailable) Error() string {
	return e.Message
}

type ErrorGatewayTimeout struct {
	Message string `json:"message"`
}

func (e ErrorGatewayTimeout) Error() string {
	return e.Message
}

type ErrorHTTPVersionNotSupported struct {
	Message string `json:"message"`
}

func (e ErrorHTTPVersionNotSupported) Error() string {
	return e.Message
}

type ErrorVariantAlsoNegotiates struct {
	Message string `json:"message"`
}

func (e ErrorVariantAlsoNegotiates) Error() string {
	return e.Message
}

type ErrorInsufficientStorage struct {
	Message string `json:"message"`
}

func (e ErrorInsufficientStorage) Error() string {
	return e.Message
}

type ErrorLoopDetected struct {
	Message string `json:"message"`
}

func (e ErrorLoopDetected) Error() string {
	return e.Message
}

type ErrorBandwidthLimitExceeded struct {
	Message string `json:"message"`
}

func (e ErrorBandwidthLimitExceeded) Error() string {
	return e.Message
}

type ErrorNotExtended struct {
	Message string `json:"message"`
}

func (e ErrorNotExtended) Error() string {
	return e.Message
}

type ErrorNetworkAuthenticationRequired struct {
	Message string `json:"message"`
}

func (e ErrorNetworkAuthenticationRequired) Error() string {
	return e.Message
}

type ErrorUnknownError struct {
	Message string `json:"message"`
}

func (e ErrorUnknownError) Error() string {
	return e.Message
}

type ErrorWebServerIsDown struct {
	Message string `json:"message"`
}

func (e ErrorWebServerIsDown) Error() string {
	return e.Message
}

type ErrorConnectionTimedOut struct {
	Message string `json:"message"`
}

func (e ErrorConnectionTimedOut) Error() string {
	return e.Message
}

type ErrorOriginIsUnreachable struct {
	Message string `json:"message"`
}

func (e ErrorOriginIsUnreachable) Error() string {
	return e.Message
}

type ErrorTimeoutOccurred struct {
	Message string `json:"message"`
}

func (e ErrorTimeoutOccurred) Error() string {
	return e.Message
}

type ErrorSSLHandshakeFailed struct {
	Message string `json:"message"`
}

func (e ErrorSSLHandshakeFailed) Error() string {
	return e.Message
}

type ErrorInvalidSSLCertificate struct {
	Message string `json:"message"`
}

func (e ErrorInvalidSSLCertificate) Error() string {
	return e.Message
}